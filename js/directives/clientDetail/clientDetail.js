MetronicApp.directive("clientDetail", function () {
    return {
        restrict: 'E',
        scope: {
            detail: '=',
        },
        link: function (scope, element, attrs, alerts) {
            ////console.log(scope, element, attrs, alerts)
        },
        templateUrl: 'js/directives/clientDetail/clientDetail.html',
        controller: ['$scope', '$http', 'settings', "$sce", '$interval', '$location', '$interval', '$timeout', 'dataToServer', function ($scope, $http, settings, $sce, $interval, $location, $interval, $timeout, dataToServer) {
            if (!$scope.detail) {
                return;
            }

            console.log($scope.detail);
            
            if ($scope.detail.vista == "crearGuia") { $scope.detail.loadTypeMaterial() }

           
            
            /* CREAR GUIA*/
            $scope.correntPage = 0;
            $scope.posicion = 10;
            $scope.pagesize = 10;
            $scope.paginas = [];

            $scope.currentPage = [0, 0];
            $scope.pageSize = ($scope.detail.pagination) ? [10, 10] : [999999, 999999];
            $scope.pages = [];


            /* PAGUINACION */
            $scope.configPages = function (x) {
                if (x) {
                    $scope.stop = $interval(function () {
                        if ($scope.detail.items && $scope.detail.items.length > 0) {
                            $interval.cancel($scope.stop);
                            $scope.stop = null;
                            $scope.Paginacion($scope.detail.items, 0);
                        }
                    }, 500);
                } else {
                    $scope.Paginacion($scope.detail.items, 1)
                }
            };
            $scope.Paginacion = function (data, i) {
                $scope.pages[i] = [];
                var ini = $scope.currentPage[i] - 4;
                var fin = $scope.currentPage[i] + 5;
                if (ini < 1) {
                    ini = 1;
                    fin = Math.ceil(data.length / $scope.pageSize[i]);
                } else if (ini >= Math.ceil(data.length / $scope.pageSize[i]) - 15) {
                    ini = Math.ceil(data.length / $scope.pageSize[i]) - 15;
                    fin = Math.ceil(data.length / $scope.pageSize[i]);

                }
                $scope.va11 = 1;
                $scope.val2 = (fin > 7) ? 7 : fin;
                $scope.FIN = fin;
                if (ini < 1)
                    ini = 1;
                for (var x = ini; x <= fin; x++) {
                    $scope.pages[i].push({
                        no: x,
                        vsbl: ((x < 9) ? false : true),
                        tresP: '...',
                        tresPV: false
                    });
                }
                angular.forEach($scope.pages[i], function (val, key) {
                    if (val.no < $scope.va11 || val.no > $scope.val2) {
                        val.tresPV = true;
                    }
                    if (val.no === fin) {
                        val.vsbl = false;
                        val.tresPV = false;
                    }
                })
                if ($scope.currentPage[i] >= $scope.pages[i].length) {
                    $scope.currentPage[i] = $scope.pages[i].length - 1;
                }
            }
            $scope.setPage = function (pg, i) {
                if (pg === $scope.val2) {
                    $scope.val2++;
                    $scope.va11++;
                } else if (pg === $scope.FIN) {
                    $scope.val2 = $scope.FIN;
                    $scope.va11 = $scope.FIN - 7;
                } else if (pg === $scope.va11) {
                    $scope.val2--;
                    $scope.va11--;
                } else if (pg === 1) {
                    $scope.val2 = 7;
                    $scope.va11 = 1;
                }
                angular.forEach($scope.pages[i], function (val, key) {
                    if (val.no < $scope.va11 || val.no > $scope.val2) {
                        val.tresPV = true;
                        val.vsbl = true;
                    } else {
                        val.tresPV = false;
                        val.vsbl = false;
                    }
                    if (val.no === $scope.FIN) {
                        val.vsbl = false;
                        val.tresPV = false;
                    }
                    if (val.no === $scope.val2) {
                        val.vsbl = false;
                        val.tresPV = false;
                    }
                    if (val.no === $scope.va11) {
                        val.vsbl = false;
                        val.tresPV = false;
                    }
                    if (val.no === ($scope.va11 - 1) || val.no === ($scope.val2 + 1)) {
                        val.vsbl = false;
                    }
                });
                $scope.currentPage[i] = pg - 1;
            };
            /* PAGUINACION */
        }]
    };
}).filter('startFromGrid', function () {
    return function (input, start) {
        if (input === undefined) return;
        try {
            start = +start;
            return input.slice(start);
        } catch (e) { }
    };
});