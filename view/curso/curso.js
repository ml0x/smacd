/* Setup blank page controller */
angular.module('MetronicApp').controller('curso_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout) {

        $scope.alert = { loading: false };
        $scope.formNewUser = false;
        $scope.tablaMantenedor = true;
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.listaDepartamentosFiltrados = [];

        $scope.data = {
            items: [],
            formulario: {
                id: "",
                nrc: "",
                nombre: "",
                departamento: "",
                estado: "",
                sede: ""
            }
        }

        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANTENEDOR_CURSO",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                // //console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    angular.forEach(response.data, function (v, k) {
                        $scope.data.items.push(v);
                    });
                    $scope.alert = { loading: false };
                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })
        }

        $scope.loadData();

        $scope.agregar = function () {

            //console.log($scope.data.formulario);
            var input = {
                SP: "MANTENEDOR_CURSO",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: $scope.data.formulario.nrc },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.departamento.objeto.id },
                    { value: $scope.data.formulario.sede.objeto.id },
                    { value: $scope.data.formulario.estado.value }
                ]
            }


            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_DEPARTAMENTO.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })

        }

        $scope.editar = function () {
            var input = {
                SP: "MANTENEDOR_CURSO",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.nrc },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.departamento.objeto.id },
                    { value: $scope.data.formulario.sede.objeto.id },
                    { value: $scope.data.formulario.estado.value }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_DEPARTAMENTO.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        }

        $scope.eliminar = function (item) {
            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANTENEDOR_CURSO",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.nrc },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        //console.log(response)
                        if (response.error == 0) {
                            $scope.alert.show = false;
                            $scope.loadData();
                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { })
                }
            }
        }

        $scope.abrirFormularioUsuario = function (opcion, item) {
            $scope.data.formulario = [];
            //console.log(item);

            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;
            (opcion == "add") ? $scope.tituloFormularioUser = "Nuevo " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            angular.forEach($rootScope.listaDepartamentos, function (v) {
                if (v.objeto.id == item.departamento) {
                    item.departamento = v;
                }
            })

            angular.forEach($rootScope.listaEstados, function (v) {
                if (v.value == item.estado) {
                    item.estado = v;
                }
            })

            if (opcion == "edit") {
                $scope.data.formulario.nrc = item.nrc;
                $scope.data.formulario.nombre = item.nombre;
                $scope.data.formulario.estado = item.estado;;

                angular.forEach($rootScope.listaSedes, function (v, k) {
                    if (v.objeto.id == item.sede) {
                        $scope.data.formulario.sede = v;
                    }
                })

                $scope.listaDepartamentosFiltrados = [];
                angular.forEach($rootScope.listaDepartamentos, function (v, k) {
                    if ($scope.data.formulario.sede.objeto.id == item.sede) {
                        $scope.listaDepartamentosFiltrados.push(v);
                    }
                    if (item.departamento.objeto.id == v.objeto.id) {
                        $scope.data.formulario.departamento = v;
                    }
                })

            }

            $scope.volverTabla = function () {
                $scope.formNewUser = false;
                $scope.mostrarGuardar = false;
                $scope.mostrarEditar = false;
                $scope.tablaMantenedor = true;
                $scope.loadData();
            }
            $scope.loadData();
        }
        
        $scope.filtrarDepartamento = function (item) {
            $scope.listaDepartamentosFiltrados = [];
            angular.forEach($rootScope.listaDepartamentos, function (v, k) {
                if (v.objeto.sede == item.objeto.id) {
                    $scope.listaDepartamentosFiltrados.push(v)
                }
            })
        }
    }
]);
