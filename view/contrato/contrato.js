/* Setup blank page controller */
angular.module('MetronicApp').controller('contrato_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout', '$filter', 'ngTableParams',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout, $filter, ngTableParams) {

        //variables
        $scope.alert = { loading: false };
        $scope.formNewUser = false;
        $scope.tablaMantenedor = true;
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.data = {
            items: [],
            formulario: {
                id: "",
                nombre: "",
                estado: ""
            }
        }
        //variables FIN

        //MANTENEDOR
        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANTENEDOR_CONTRATO",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    angular.forEach(response.data, function (v, k) {
                        $scope.data.items.push(v);
                    });

                    $scope.taskCheckbox = { "isNormal": true };
                    $scope.taskDetailData = $scope.data.items

                    $scope.resetNgTable = function () {
                        return {
                            total: $scope.taskDetailData.length, // length of data
                            getData: function ($defer, params) {
                                var filters = params.filter();
                                var orderedData = params.sorting() ? $filter('orderBy')($scope.taskDetailData, params.orderBy()) : $scope.taskDetailData;
                                orderedData = $filter('filter')(orderedData, filters);

                                var table_data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                params.total(orderedData.length); // set total for recalc pagination
                                $defer.resolve(table_data);
                                $scope.alert = { loading: false };
                            }
                        }
                    }

                    $scope.taskDetailTableParams = new ngTableParams({
                        page: 1, // show first page
                        count: 10, // count per page
                        sorting: {
                            index: 'asc' // initial sorting
                        }
                    }, $scope.resetNgTable());

                    setTimeout(() => {
                        $scope.taskDetailTableParams.sorting({});
                        $scope.$apply();
                        $scope.alert = { loading: false };
                    }, 500);
                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })
        }
        $scope.loadData();

        $scope.agregar = function () {

            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANTENEDOR_CONTRATO",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: null },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.estado.value }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_CONTRATO.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })

        }

        $scope.editar = function () {

            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANTENEDOR_CONTRATO",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.id },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.estado.value }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_CONTRATO.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        }

        $scope.eliminar = function (item) {
            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANTENEDOR_CONTRATO",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.id },
                            { value: null },
                            { value: null },
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        //console.log(response)
                        if (response.error == 0) {
                            $scope.alert.show = false;
                            $scope.loadData();
                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { })
                }
            }
        }
        //MANTENEDOR FIN

        //FUNCIONES
        $scope.validarFormulario = function () {
            var validar = true;

            if ($scope.data.formulario.nombre == "" || $scope.data.formulario.nombre == undefined || $scope.data.formulario.nombre == null && validar) {
                validar = false;
            }
            if ($scope.data.formulario.estado == "" || $scope.data.formulario.estado == undefined || $scope.data.formulario.estado == null && validar) {
                validar = false;
            }

            return validar;
        }

        $scope.abrirFormularioUsuario = function (opcion, item) {
            $scope.data.formulario = [];

            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;
            (opcion == "add") ? $scope.tituloFormularioUser = "Nuevo " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            angular.forEach($rootScope.listaEstados, function (v) {
                if (v.value == item.estado) {
                    item.estado = v;
                }
            })

            if (opcion == "edit") {
                $scope.data.formulario.id = item.id;
                $scope.data.formulario.nombre = item.nombre;
                $scope.data.formulario.estado = item.estado;
            }

            $scope.volverTabla = function () {
                $scope.formNewUser = false;
                $scope.mostrarGuardar = false;
                $scope.mostrarEditar = false;
                $scope.tablaMantenedor = true;
                $scope.loadData();
            }
            $scope.loadData();
        }
        //FUNCIONES FIN
    }
]);
