/***
Metronic AngularJS App Main Script
***/




/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "ngTable"
]);

var IPSERVICES = "http://200.55.206.139:8081/JwtRfc2json/";
//var IPSERVICES2 = "http://200.55.206.139:8081/didd/";
var token = "";


/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout',
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive


    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        setTimeout(function () {
            QuickSidebar.init(); // init quick sidebar
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/*Set services */
MetronicApp.service('dataToServer', function ($http, $q, $rootScope) {
    return {
        SERVELET1: {
            //Desarrollo
            "IPDES": "http://10.99.99.96:8080/JwtRfc2json/"
        },
        SERVELET: {
            //Desarrollo
            "IPDES": "http://10.99.99.96:8080/JwtRfc2json/"
        },
        sendToServelet: function (p, c) { //postData, rquestConfg
            var deferred = $q.defer();
            c = c || {};
            ////console.log(p,c.method || 'POST' );
            App.startPageLoading({ animate: true, message: c.message || "Enviando Datos a Hana" });
            return $http({
                url: c.url || this.SERVELET.IPDES + '/calidadSC/' + (c.servelet || 'ManageReport'),
                //url:'http://hortifrut.expled.cl/calidad/origen_getDataBySP.aspx?SP=PC_MEX_TEMPERATURA&FILTER=[{"item":"tipo","type":"string","value":""},{"item":"semana","type":"int","value":-1},{"item":"acopio","type":"string","value":"-1"},{"item":"productor","type":"string","value":"-1"},{"item":"especie","type":"string","value":"ARANDANO"},{"item":"variedad","type":"string","value":"-1"},{"item":"season","type":"string","value":"1718"}]',
                //contentType: c.contentType || 'application/json; charset=utf-8',
                method: c.method || 'POST',
                // dataType: c.dataType || 'json',
                //data:'data='+encodeURIComponent(JSON.stringify(c.data||$scope.oform||{})),
                data: "&" + encodeURI(p),
                //data:'data=rr',
                timeout: 120000,
                headers: c.headers || { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
            }).success(function (data) {
                App.stopPageLoading();
                deferred.resolve(data);
                ////console.log(data);
                return data;
            }).error(function (err) {
                App.stopPageLoading();
                deferred.reject('There was an error');
                //console.log(err);
                return err;
            });
            return deferred.promise;
        },
        send2ServletExpo: function (p, c) { //postData, rquestConfg
            var deferred = $q.defer();
            c = c || {};
            App.startPageLoading({ animate: ((c.animate !== undefined) ? c.animate : true), message: c.message || "Enviando Datos" });
            return $http({
                url: c.url || this.SERVELET.IPDES + (c.path || '/calidadSC/SetupExportadora'),
                method: c.method || 'POST',
                dataType: c.dataType || 'text',
                data: '&data=' + encodeURI(JSON.stringify(p || {})),
                timeout: 120000,
                headers: c.headers || { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
            }).success(function (data) {
                App.stopPageLoading();
                deferred.resolve(data);
                ////console.log(data);
                return data;
            }).error(function (err) {
                App.stopPageLoading();
                deferred.reject('There was an error');
                //console.log(err);
                return err;
            });
            return deferred.promise;
        },

    };
});
/*Fin services */

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        // Redirect any unmatched url
        $urlRouterProvider.otherwise("/login");

        $stateProvider

            // Dashboard
            .state('dashboard', {
                url: "/dashboard.html",
                templateUrl: "views/dashboard.html",
                data: { pageTitle: 'Admin Dashboard Template' },
                controller: "DashboardController",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/global/plugins/morris/morris.css',
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',
                                '../assets/global/plugins/jquery.sparkline.min.js',
                                '../assets/pages/scripts/dashboard.min.js',
                                'js/controllers/DashboardController.js',
                            ]
                        });
                    }]
                }
            })

            // Blank Page
            .state('blank', {
                url: "/blank",
                templateUrl: "views/blank.html",
                data: { pageTitle: 'Blank Page Template' },
                controller: "BlankController",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'js/controllers/BlankController.js'
                            ]
                        });
                    }]
                }
            })
            .state("login", {
                url: "/login",
                templateUrl: "view/login/login.html",
                data: { pageTitle: 'DIDD UNAB' },
                controller: "Login",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/login/login.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                            ]
                        });
                    }]
                }

            })


            .state("menu", {
                url: "/menu",
                templateUrl: "view/menu/menu.html",
                data: { pageTitle: 'Menu Principal - Despacho Goplicity' },
                controller: "Menu",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/menu/menu.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("profesor", {
                url: "/profesor",
                templateUrl: "view/profesor/profesor.html",
                data: { pageTitle: 'Profesores - DIDD' },
                controller: "profesor_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/profesor/profesor.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("departamento", {
                url: "/departamento",
                templateUrl: "view/departamento/departamento.html",
                data: { pageTitle: 'Departamento - DIDD' },
                controller: "departamento_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/departamento/departamento.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("curso", {
                url: "/curso",
                templateUrl: "view/curso/curso.html",
                data: { pageTitle: 'Cursos - DIDD' },
                controller: "curso_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/curso/curso.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("sede", {
                url: "/sede",
                templateUrl: "view/sede/sede.html",
                data: { pageTitle: 'Sede - DIDD' },
                controller: "sede_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/sede/sede.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("nivel_academico", {
                url: "/nivel_academico",
                templateUrl: "view/nivel_academico/nivel_academico.html",
                data: { pageTitle: 'Nivel Academico- DIDD' },
                controller: "nivel_academico_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/nivel_academico/nivel_academico.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("contrato", {
                url: "/contrato",
                templateUrl: "view/contrato/contrato.html",
                data: { pageTitle: 'Contrato - DIDD' },
                controller: "contrato_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/contrato/contrato.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("capacitacion", {
                url: "/capacitacion",
                templateUrl: "view/capacitacion/capacitacion.html",
                data: { pageTitle: 'Capacitacion - DIDD' },
                controller: "capacitacion_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/capacitacion/capacitacion.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("grupo_capacitacion", {
                url: "/grupo_capacitacion",
                templateUrl: "view/grupo_capacitacion/grupo_capacitacion.html",
                data: { pageTitle: 'Grupo Capacitacion - DIDD' },
                controller: "grupo_capacitacion_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/grupo_capacitacion/grupo_capacitacion.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("perfil", {
                url: "/perfil",
                templateUrl: "view/perfil/perfil.html",
                data: { pageTitle: 'Capacitacion - DIDD' },
                controller: "perfil_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/perfil/perfil.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("asignar_capacitacion", {
                url: "/asignar_capacitacion",
                templateUrl: "view/asignar_capacitacion/asignar_capacitacion.html",
                data: { pageTitle: 'Asignacion de capacitaciones - DIDD' },
                controller: "asignar_capacitacion_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/asignar_capacitacion/asignar_capacitacion.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })

            .state("mis_capacitaciones", {
                url: "/mis_capacitaciones",
                templateUrl: "view/mis_capacitaciones/mis_capacitaciones.html",
                data: { pageTitle: 'Mis Capacitaciones - DIDD' },
                controller: "mis_capacitaciones_ctr",
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'MetronicApp',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                'view/mis_capacitaciones/mis_capacitaciones.js',
                                'js/directives/ag1Alert/ag1Alert.js',
                                'js/directives/rowHeader/rowHeader.js',
                            ]
                        });
                    }]
                }
            })
    }
]);

MetronicApp.run(["$rootScope", "settings", "$state", "$location", function ($rootScope, settings, $state, $location) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view

    if ($location.$$path != "/login") {
        $location.path("/login");
    }

}]);

MetronicApp.factory('$exceptionHandler', function ($log) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        //console.log(exception)
        //console.log({exception:exception})
        alert(exception.stack + ' <-->' + cause);
    };
});
