/* Setup blank page controller */
angular.module('MetronicApp').controller('grupo_capacitacion_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout', '$filter', 'ngTableParams',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout, $filter, ngTableParams) {

        //VARIABLES
        $scope.alert = { loading: false };
        $scope.formNewUser = false;
        $scope.tablaMantenedor = true;
        $scope.formExcel = false;
        $scope.formDocument = false;
        $scope.documentForm = false;
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.data = {
            items: [],
            formulario: {
                id: "",
                descripcion: ""
            }
        }
        //VARIABLES FIN

        ///// FUNCIONALIDAD /////
        $scope.abrirFormularioUsuario = function (opcion, item) {
            $scope.data.formulario = [];
            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;

            (opcion == "add") ? $scope.tituloFormularioUser = "Nueva " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            if (opcion == "edit") {
                $scope.data.formulario.id = item.id;
                $scope.data.formulario.descripcion = item.descripcion;
            }

            $scope.volverTabla = function () {
                $scope.formNewUser = false;
                $scope.mostrarGuardar = false;
                $scope.mostrarEditar = false;
                $scope.tablaMantenedor = true;
                $scope.taskDetailTableParams.sorting({});
                $scope.loadData();
            }

            $scope.loadData();
            $rootScope.sendRequest = function (json) {
                delete json.data.INPUT;
                delete json.data.TABLES;
                var resx = {}
                return resx;
            }
        }
        ///// FUNCIONALIDAD FIN/////

        ///// VALIDACION /////
        $scope.validarFormulario = function () {
            var valida = true;
            if ($scope.data.formulario.descripcion == "" || $scope.data.formulario.descripcion == null || $scope.data.formulario.descripcion == undefined && valida) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Para Continuar debes completar el campo Capacitacion.",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                valida = false;
            }

            return valida;
        }
        ///// VALIDACION FIN/////

        ///// MANTENEDOR /////
        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANTENEDOR_GRUPO",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null }
                ]
            }

            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    $scope.data.items = response.data;

                    $scope.taskCheckbox = { "isNormal": true };
                    $scope.taskDetailData = $scope.data.items

                    $scope.resetNgTable = function () {
                        return {
                            total: $scope.taskDetailData.length, // length of data
                            getData: function ($defer, params) {
                                var filters = params.filter();
                                var orderedData = params.sorting() ? $filter('orderBy')($scope.taskDetailData, params.orderBy()) : $scope.taskDetailData;
                                orderedData = $filter('filter')(orderedData, filters);

                                var table_data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                params.total(orderedData.length); // set total for recalc pagination
                                $defer.resolve(table_data);
                                $scope.alert = { loading: false };
                            }
                        }
                    }

                    $scope.taskDetailTableParams = new ngTableParams({
                        page: 1, // show first page
                        count: 10, // count per page
                        sorting: {
                            index: 'asc' // initial sorting
                        }
                    }, $scope.resetNgTable());

                    setTimeout(() => {
                        $scope.taskDetailTableParams.sorting({});
                        $scope.$apply();
                        $scope.alert = { loading: false };
                    }, 500);

                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })
        }
        $scope.loadData();

        $scope.agregar = function () {
            var valida = $scope.validarFormulario();
            if (valida) {
                var input = {
                    SP: "MANTENEDOR_GRUPO",
                    FILTERS: [
                        { value: 1, type: "number" },
                        { value: null },
                        { value: $scope.data.formulario.descripcion }
                    ]
                }

                $http({
                    url: IPSERVICES + "post",
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    data: JSON.stringify(input),
                    headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                }).success(function (response) {
                    //console.log(response)
                    if (response.error == 0) {
                        $scope.alert = {
                            show: true,
                            title: "Mensaje",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.data[0].message,
                            close: function () {
                                $scope.alert.show = false;
                                $scope.volverTabla();
                            }
                        }

                    } else {
                        $scope.alert = {
                            show: true,
                            title: "Alerta",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.message,
                            close: function () {
                                $scope.alert.show = false;
                            }
                        }
                    }
                }).error(function (e) {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: 'Error de conexion con servicio MANTENEDOR_GRUPO.',
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                })
            }
        };

        $scope.editar = function () {

            var input = {
                SP: "MANTENEDOR_GRUPO",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.id },
                    { value: $scope.data.formulario.descripcion }
                ]
            }

            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_GRUPO.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        }

        $scope.eliminar = function (item) {

            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.descripcion + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANTENEDOR_GRUPO",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.id },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        // //console.log(response)
                        if (response.error == 0) {
                            $scope.alert.show = false;
                            $scope.loadData();
                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { })
                }
            }
        }
        ///// MANTENEDOR FIN/////

    }
]);
