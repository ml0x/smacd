$(function()
{
	$('.modal-opener').on('click', function()
	{
		if( !$('#goplicity-form-modal-overlay').length )
		{
			$('body').append('<div id="goplicity-form-modal-overlay" class="goplicity-form-modal-overlay"></div>');
		}		
	
		$('#goplicity-form-modal-overlay').on('click', function()
		{
			$('#goplicity-form-modal-overlay').fadeOut();
			$('.goplicity-form-modal').fadeOut();
		});
		
		form = $($(this).attr('href'));
		$('#goplicity-form-modal-overlay').fadeIn();
		form.css('top', '50%').css('left', '50%').css('margin-top', -form.outerHeight()/2).css('margin-left', -form.outerWidth()/2).fadeIn();
		
		return false;
	});
	
	$('.modal-closer').on('click', function()
	{
		$('#goplicity-form-modal-overlay').fadeOut();
		$('.goplicity-form-modal').fadeOut();
		
		return false;
	});
});