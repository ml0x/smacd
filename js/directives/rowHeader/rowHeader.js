MetronicApp.directive("rowHeader", function () {
    return {
        restrict: 'E',
        scope: {
            row: '='
        },
        link: function (scope, element, attrs, alerts) {
            ////console.log(scope, element, attrs, alerts)
        },
        templateUrl: 'js/directives/rowHeader/rowHeader.html',
        controller: ['$scope', '$http', '$interval', '$location', function ($scope, $http, $interval, $location) {
            if (!$scope.row) {
                return;
            }

            $scope.irOpcion = function (dir) {
                $location.path("/" + dir)
            }
            
        }]
    };
});
