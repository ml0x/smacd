/* Setup blank page controller */
angular.module('MetronicApp').controller('perfil_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout) {
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.mostarFormulario = false;
        $scope.mensajeContrasena = "";
        $scope.data = {
            formulario: {
                user: $rootScope.datosUsuario.user,
                pass: $rootScope.datosUsuario.pass,
                pass2: $rootScope.datosUsuario.pass,
                rut: $rootScope.datosUsuario.rut,
                nombre: $rootScope.datosUsuario.nombre,
                correo: $rootScope.datosUsuario.correo,
                telefono: $rootScope.datosUsuario.telefono,
                nivel_academico: "",
                departamento: "",
                estado: $rootScope.datosUsuario.estado,
                capacitacion: ""
            }
        }


        $scope.validarFormulario = function () {
            var validar = true;

            if ($scope.data.formulario.nombre == null || $scope.data.formulario.nombre == "" || $scope.data.formulario.nombre == undefined && validar) {
                validar = false;
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Para guardar debes editar el campo NOMBRE",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            }

            if ($scope.data.formulario.pass == null || $scope.data.formulario.pass == "" || $scope.data.formulario.pass == undefined || $scope.data.formulario.pass2 == null || $scope.data.formulario.pass2 == "" || $scope.data.formulario.pass2 == undefined && validar) {
                var regexPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
                if (validar) {
                    validar = regexPass.test($scope.data.formulario.pass)
                    console.log("regex resultado", validar);
                }
                if (!validar) {
                    $scope.alert = {
                        show: true,
                        title: "Error",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "Las contraseñas deben coincidir y cumplir con los parametros establecidos antes de continuar.",
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }


            if ($scope.data.formulario.pass != $scope.data.formulario.pass2 && validar) {
                validar = false;
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Las contraseñas deben coincidir.",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            }



            if ($scope.data.formulario.correo == null || $scope.data.formulario.correo == "" || $scope.data.formulario.correo == undefined && validar) {
                validar = false;
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Para guardar debes editar el campo CORREO",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            }

            if ($scope.data.formulario.telefono == null || $scope.data.formulario.telefono == "" || $scope.data.formulario.telefono == undefined && validar) {
                validar = false;
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Para guardar debes editar el campo TELEFONO",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            }


            console.log($scope.data.formulario);

            return validar;
        }

        $scope.validarCoincidencia = function () {
            if ($scope.data.formulario.pass !== $scope.data.formulario.pass2) {
                $scope.color = "red";
                $scope.mensajeContrasena = "La contraseña NO coincide";
            } else {
                $scope.color = "green";
                $scope.mensajeContrasena = "La contraseña coincide";
            }
        }

        $scope.editar = function () {
            var validar = $scope.validarFormulario();
            if (validar) {
                var input = {
                    SP: "MANTENEDOR_PROFESOR",
                    FILTERS: [
                        { value: 4, type: "number" },
                        { value: $scope.data.formulario.rut },
                        { value: $scope.data.formulario.nombre },
                        { value: $scope.data.formulario.correo },
                        { value: $scope.data.formulario.telefono },
                        { value: 0, type: "number" },
                        { value: 0, type: "number" },
                        { value: 0, type: "number" },
                        { value: $scope.data.formulario.user },
                        { value: $scope.data.formulario.pass },
                        { value: 0, type: "number" }
                    ]
                }

                $http({
                    url: IPSERVICES + "post",
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    data: JSON.stringify(input),
                    headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                }).success(function (response) {
                    if (response.error == 0) {

                        $rootScope.datosUsuario.nombre = $scope.data.formulario.nombre;
                        $rootScope.datosUsuario.correo = $scope.data.formulario.correo;
                        $rootScope.datosUsuario.telefono = $scope.data.formulario.telefono;
                        $rootScope.datosUsuario.pass = $scope.data.formulario.pass;

                        $scope.alert = {
                            show: true,
                            title: "Mensaje",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.data[0].message,
                            close: function () {
                                $scope.alert.show = false;
                                $location.path("/menu");
                            }
                        }
                    } else {
                        $scope.alert = {
                            show: true,
                            title: "Alerta",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.message,
                            close: function () {
                                $scope.alert.show = false;
                            }
                        }
                    }

                }).error(function (e) {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: 'Error de conexion con servicio MANTENEDOR_CAPACITACION.',
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                })
            }
        };


    }
]);
