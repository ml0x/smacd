/* Setup blank page controller */
angular.module('MetronicApp').controller('profesor_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout) {
        $scope.alert = { loading: false };
        $scope.formNewUser = false;
        $scope.tablaMantenedor = true;
        $scope.formExcel = false;
        $scope.DATO_XSLS = [];
        $scope.cabecera = $rootScope.datosUsuario;

        $scope.traerFechaHoy = function () {
            var fecha = new Date();
            return fecha.getFullYear() + "-" + ((fecha.getMonth() * 1) + 1) + "-" + fecha.getDate();
        }

        $scope.data = {
            items: [],
            formulario: {
                id: "",
                nombre: "",
                apeliidO: '',
                correo: '',
                telefono: '',
                departamento: '',
                estado: ''
            }
        }

        var obtenerLargo = function (string) {
            return string.length;
        }

        $scope.validaLargo = function (opcion) {
            console.log(opcion);
            if (opcion == 'sociedad') {
                if (obtenerLargo($scope.data.formulario.sociedad) > 4) {
                    $scope.data.formulario.sociedad = $scope.data.formulario.sociedad.substring(0, 4);
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "El campo Sociedad no puede tener un largo mayor a 4.",
                        close: function () {
                            $scope.alert.show = false;

                        }
                    }
                }

            }
            if (opcion == 'puesto') {
                if (obtenerLargo($scope.data.formulario.puesto_expedicion) > 4) {
                    $scope.data.formulario.puesto_expedicion = $scope.data.formulario.puesto_expedicion.substring(0, 4);
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "El campo Puesto de expedición no puede tener un largo mayor a 4.",
                        close: function () {
                            $scope.alert.show = false;

                        }
                    }
                }
            }
            if (opcion == 'organizacion') {
                if (obtenerLargo($scope.data.formulario.organizacion_ventas) > 4) {
                    $scope.data.formulario.organizacion_ventas = $scope.data.formulario.organizacion_ventas.substring(0, 4);
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "El campo Organización de ventas no puede tener un largo mayor a 4.",
                        close: function () {
                            $scope.alert.show = false;

                        }
                    }
                }
            }
            if (opcion == 'venta') {
                if (obtenerLargo($scope.data.formulario.pedido_ventas) > 10) {
                    $scope.data.formulario.pedido_ventas = $scope.data.formulario.pedido_ventas.substring(0, 4);
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "El campo Organización de ventas no puede tener un largo mayor a 10.",
                        close: function () {
                            $scope.alert.show = false;

                        }
                    }
                }
            }
        };


        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    angular.forEach(response.data, function (v, k) {
                        $scope.data.items.push(v);
                    });
                    $scope.alert = { loading: false };
                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })
        }

        $scope.loadData();

        $scope.agregar = function () {
            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: null },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.apellidos },
                    { value: $scope.data.formulario.correo },
                    { value: $scope.data.formulario.telefono },
                    { value: $scope.data.formulario.departamento },
                    { value: $scope.data.formulario.estado }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                console.log(response)
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_PROFESOR.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })

        }

        $scope.editar = function () {

            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.id },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.apellidos },
                    { value: $scope.data.formulario.correo },
                    { value: $scope.data.formulario.telefono },
                    { value: $scope.data.formulario.departamento },
                    { value: $scope.data.formulario.estado }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_PROFESOR.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        }

        $scope.eliminar = function (item) {

            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANTENEDOR_PROFESOR",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.id },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        console.log(response)
                        if (response.error == 0) {
                            $scope.alert.show = false;
                            $scope.loadData();
                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { })
                }
            }
        }

        $scope.abrirFormExcel = function () {
            $scope.formExcel = true;
            $scope.tablaMantenedor = false;
        }

        $scope.cerrarFormExcel = function () {
            $scope.formExcel = false;
            $scope.tablaMantenedor = true;
            $scope.loadData();
        }

        $scope.abrirFormularioUsuario = function (opcion, item) {
            $scope.data.formulario = [];

            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;
            (opcion == "add") ? $scope.tituloFormularioUser = "Nuevo " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            if (opcion == "edit") {
                $scope.data.formulario.id = item.id;
                $scope.data.formulario.nombre = item.nombre;
                $scope.data.formulario.apellidos = item.apellidos;
                $scope.data.formulario.correo = item.correo;
                $scope.data.formulario.telefono = item.telefono;
                $scope.data.formulario.departamento = item.departamento;
                $scope.data.formulario.estado = item.estado;


                /*angular.forEach($rootScope.listaPerfiles, function (v, k) {
                    if (v.objeto.id == item.perfil) {
                        $scope.data.formulario.perfil = v;
                    }
                })*/
            }

            $scope.volverTabla = function () {
                $scope.formNewUser = false;
                $scope.mostrarGuardar = false;
                $scope.mostrarEditar = false;
                $scope.tablaMantenedor = true;
                $scope.loadData();
            }

            $scope.loadData();

            $rootScope.sendRequest = function (json) {
                delete json.data.INPUT;
                delete json.data.TABLES;
                var resx = {}
                return resx;
            }

            $scope.cargarExcel = function () {
                $scope.DATO_XSLS = [];
                try {
                    var input = event.target;
                    var reader = new FileReader();
                    setTimeout(function () {
                        reader.onload = function () {
                            var fileData = reader.result;
                            var wb = XLSX.read(fileData, { type: 'binary' });
                            wb.SheetNames.forEach(function (sheetName) {
                                $scope.DATO_XSLS.push(XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]));
                            })
                        };
                        try { reader.readAsBinaryString(input.files[0]) } catch (error) { }

                    }, 50);
                } catch (error) { }
            }

            $scope.log = [];
            $scope.Envio_Datos = function () {
                $scope.log = [];
                $scope.alert = { loading: true };
                sendData($scope.DATO_XSLS[0], 0, $scope.DATO_XSLS[0].length);
            }

            var sendData = function (envio, index, largo) {
                var input = {
                    SP: "MANT_USUARIO",
                    FILTERS: [
                        { value: 1, type: "number" },
                        { value: null },
                        { value: envio[index].nombre },
                        { value: envio[index].usuario },
                        { value: envio[index].clave },
                        { value: envio[index].correo },
                        { value: envio[index].telefono },
                        { value: envio[index].funcion },
                        { value: envio[index].perfil },
                        { value: envio[index].sociedad },
                        { value: envio[index].desc_sociedad },
                        { value: envio[index].rut_empresa },
                        { value: envio[index].usuario_sap },
                        { value: envio[index].clave_sap },
                        { value: envio[index].centro_costo },
                        { value: envio[index].puesto_expedicion },
                        { value: envio[index].organizacion_ventas },
                        { value: envio[index].date_pass }
                    ]
                }
                $http({
                    url: IPSERVICES + "post",
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    data: JSON.stringify(input),
                    headers: { 'Content-Type': "application/json", 'connection-properties': 'GoplicityDespachos' }
                }).success(function (response) {
                    console.log(response);

                    if (response.error != 0) {
                        $scope.log.push({ respuesta: response, datos: envio[index] })
                    }
                    index++;
                    if (index < largo) {
                        sendData(envio, index, largo);
                    } else {
                        $scope.alert = { loading: false };
                        if ($scope.log.length > 0) {
                            var mensaje = "";
                            $scope.respuesta = [];
                            angular.forEach($scope.log, function (v, k) {
                                mensaje = "- El usuario " + v.datos.usuario + " no se ha creado debido a el error de formato de entrada o existencia del mismo.";
                                $scope.respuesta.push({ message: mensaje })
                            })

                            $scope.alert = {
                                success: true,
                                title: "Error",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                structMessage: $scope.respuesta,
                                close: function () {
                                    $scope.alert.success = false;
                                }
                            }

                        } else {
                            $scope.alert = {
                                show: true,
                                title: "Mensaje",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: "Usuarios creados correctamente.",
                                close: function () {
                                    $scope.alert.show = false;
                                }
                            }
                        }
                    }
                }).error(function (e) { })
            }


            $scope.validarContraseña = function (contraseña) {
                var validacion = false;
                var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
                validacion = regex.test(contraseña)
                return validacion;
            }




            $scope.descargarUsuarios = function () {
                $scope.alert = {
                    show: true,
                    title: "Mensaje",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: true,
                    buttonClose: true,
                    message: "¿Deseas descargar la lista de usuarios en formato excel?",
                    accept: function () {

                        $scope.alert = { loading: true };

                        var input = {
                            SP: "MANT_USUARIO",
                            FILTERS: [
                                { value: 0, type: "number" },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null },
                                { value: null }
                            ]
                        }

                        $http({
                            url: IPSERVICES + "post",
                            method: 'POST',
                            dataType: 'json',
                            timeout: 10000,
                            data: JSON.stringify(input),
                            headers: { 'Content-Type': "application/json", 'connection-properties': 'GoplicityDespachos' }
                        }).success(function (response) {
                            if (response.error == 0 && response.data.length > 0) {

                                var createXLSLFormatObj = [];

                                /* XLS Head Columns */
                                var xlsHeader = ["id", "nombre", "usuario", "clave", "correo", "telefono", "funcion", "perfil", "sociedad", "desc_sociedad", "rut_empresa", "usuario_sap", "clave_sap", "centro_costo", "puesto_expedicion", "organizacion_ventas"];

                                /* XLS Rows Data */
                                var xlsRows = [];

                                angular.forEach(response.data, function (v, k) {
                                    xlsRows.push(
                                        {
                                            "id": v.id,
                                            "nombre": v.nombre,
                                            "usuario": v.usuario,
                                            "clave": v.clave,
                                            "correo": v.correo,
                                            "telefono": v.telefono,
                                            "funcion": v.funcion,
                                            "perfil": v.perfil,
                                            "sociedad": v.sociedad,
                                            "desc_sociedad": v.desc_sociedad,
                                            "rut_empresa": v.rut_empresa,
                                            "usuario_sap": v.usuario_sap,
                                            "clave_sap": v.clave_sap,
                                            "centro_costo": v.centro_costo,
                                            "puesto_expedicion": v.puesto_expedicion,
                                            "organizacion_ventas": v.organizacion_ventas,
                                            "date_pass": v.date_pass
                                        }
                                    )
                                })


                                createXLSLFormatObj.push(xlsHeader);
                                $.each(xlsRows, function (index, value) {
                                    var innerRowData = [];
                                    // $("tbody").append('<tr><td>' + value.EmployeeID + '</td><td>' + value.FullName + '</td></tr>');
                                    $.each(value, function (ind, val) {
                                        innerRowData.push(val);
                                    });
                                    createXLSLFormatObj.push(innerRowData);
                                });


                                /* File Name */
                                var filename = "usuarios_mantenedor_despacho.xlsx";

                                /* Sheet Name */
                                var ws_name = "Usuarios";

                                if (typeof console !== 'undefined') console.log(new Date());
                                var wb = XLSX.utils.book_new(),
                                    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

                                /* Add worksheet to workbook */
                                XLSX.utils.book_append_sheet(wb, ws, ws_name);



                                /* Write workbook and Download */
                                if (typeof console !== 'undefined') console.log(new Date());
                                XLSX.writeFile(wb, filename);
                                if (typeof console !== 'undefined') console.log(new Date());

                                $scope.alert = { loading: false };

                            } else {
                                alert(response.message);
                                $scope.alert = { loading: false };
                            }
                        }).error(function (e) { })

                    },
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            }
        }
    }
]);
