MetronicApp.directive("ag1Login", function () {
    return {
        restrict: 'E',
        scope: {
            login: '='
        },
        link: function (scope, element, attrs, alerts) {
            //console.log(scope, element, attrs, alerts)
        },
        templateUrl: 'js/directives/login/login.html',
        controller: ['$scope', '$http', '$interval', '$location', function ($scope, $http, $interval, $location) {
            //console.log($scope)
            $scope.alerta = {
                call: {}
            };

            $scope.alerta.call = function (fn, x, x2) {
                //console.log(fn, x, x2);
                $scope[fn](x, x2);
            };

            $scope.logear = function () {

                $location.path('/menu');
            }

            $scope.login = [
                { class: "input_large center-block", model: "", type: "text", placeholder: "Ingrese Usuario", style: "margin-bottom: 20px;" },
                { class: "input_large center-block", model: "", type: "password", placeholder: "Ingrese Contraseña", style: "margin-bottom: 20px;" },
                { class: "input_large center-block", model: "", type: "text", placeholder: "Ingrese Centro", style: "margin-bottom: 20px;" },
            ]
            //console.log($scope.login);
        }]
    };
});