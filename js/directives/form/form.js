MetronicApp.directive("ag1Form", function () {
    return {
        restrict: 'E',
        scope: {
            form: '='
        },
        link: function (scope, element, attrs, alerts) {
            //console.log(scope, element, attrs, alerts);
        },
        templateUrl: 'js/directives/form/form.html',
        controller: ['$scope', '$http', '$interval', function ($scope, $http, $interval) {
            $scope.alerta = {
                show: false,
                call: {}
            }

        }]
    };
});