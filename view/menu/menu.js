/* Setup blank page controller */
angular.module('MetronicApp').controller('Menu', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location',
    function ($rootScope, $scope, settings, $http, $sce, $location) {
        $scope.alert = { loading: false };
        $scope.alerta = {
            call: {}
        };
        $rootScope.fondoApp = "bg-red";
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.irOpcion = function (dir) {
            $location.path("/" + dir)
        }
    }
]
);