/* Setup blank page controller */
angular.module('MetronicApp').controller('asignar_capacitacion_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout', '$filter', 'ngTableParams',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout, $filter, ngTableParams) {
        //VARIABLES
        $scope.alert = { loading: true };
        $scope.formNewUser = false;
        $scope.tablaMantenedor = true;
        $scope.formExcel = false;
        $scope.DATO_XSLS = [];
        $scope.formDocument = false;
        $scope.documentForm = false;
        $scope.camposEditar = false;
        $scope.capacitacionSeleccionada = "";
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.data = {
            items: [],
            formulario: {
                id: "",
                profesor: "",
                nombre: '',
                institucion: '',
                horas: '',
                fecha: '',
                documento: '',
                capacitacion: '',
                estado: ''

            }
        };
        //VARIABLES FIN

        //MANTENEDOR
        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    angular.forEach(response.data, function (v, k) {
                        var nombre_profesor = "";
                        var nombre_capacitacion = "";
                        angular.forEach($rootScope.listaProfesores, function (profesor) {
                            if (v.id_profesor == profesor.objeto.rut) {
                                nombre_profesor = profesor.objeto.nombre;
                            }
                        })

                        angular.forEach($rootScope.listaCapacitacion, function (capacitacion) {
                            if (v.id_capacitacion == capacitacion.objeto.id) {
                                nombre_capacitacion = capacitacion.objeto.descripcion;
                            }
                        })

                        $scope.data.items.push(
                            {
                                id: v.id * 1,
                                id_profesor: v.id_profesor,
                                nombre_profesor: nombre_profesor,
                                id_capacitacion: v.id_capacitacion,
                                nombre_capacitacion: nombre_capacitacion,
                                institucion: v.institucion,
                                horas: v.horas * 1,
                                fecha: v.fecha,
                                estado: v.estado,
                                desEstado: $scope.selectEstado(v.estado)
                            }
                        )
                        console.log($scope.data.items)

                    });


                    $scope.taskCheckbox = { "isNormal": true };
                    $scope.taskDetailData = $scope.data.items;

                    $scope.resetNgTable = function () {
                        return {
                            total: $scope.taskDetailData.length, // length of data
                            getData: function ($defer, params) {
                                var filters = params.filter();
                                var tempDateFilter;
                                var orderedData = params.sorting() ? $filter('orderBy')($scope.taskDetailData, params.orderBy()) : $scope.taskDetailData;

                                if (filters) {
                                    if (filters.fecha) {
                                        orderedData = $filter('customUserDateFilter')(orderedData, filters.fecha);
                                        tempDateFilter = filters.fecha;
                                        delete filters.fecha;
                                    }

                                    orderedData = $filter('filter')(orderedData, filters);
                                    filters.fecha = tempDateFilter;
                                }

                                var table_data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                params.total(orderedData.length); // set total for recalc pagination
                                $defer.resolve(table_data);
                                $scope.alert = { loading: false };
                            }
                        }
                    }

                    $scope.taskDetailTableParams = new ngTableParams({
                        page: 1, // show first page
                        count: 10, // count per page
                        sorting: {
                            index: 'asc' // initial sorting
                        }
                    }, $scope.resetNgTable());

                    setTimeout(() => {
                        $scope.taskDetailTableParams.sorting({});
                        $scope.$apply();
                        $scope.alert = { loading: false };
                    }, 500);

                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })
        };

        $scope.loadData();

        $scope.agregar = function () {

            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: null },
                    { value: $scope.data.formulario.capacitacion.objeto.id },
                    { value: $scope.data.formulario.profesor.objeto.rut },
                    { value: $scope.data.formulario.institucion },
                    { value: $scope.data.formulario.horas },
                    { value: $scope.traerFechaHoy() },
                    { value: $scope.data.formulario.documento },
                    { value: $scope.data.formulario.estado.value },
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                console.log(response)
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_CAPACITACION.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        };

        $scope.editar = function () {
            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.id },
                    { value: $scope.data.formulario.capacitacion },
                    { value: $scope.data.formulario.profesor },
                    { value: $scope.data.formulario.institucion },
                    { value: $scope.data.formulario.horas },
                    { value: $scope.traerFechaHoy() },
                    { value: ($scope.data.formulario.documento) ? $scope.data.formulario.documento : null },
                    { value: $scope.data.formulario.estado.value },
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_CAPACITACION.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        };

        $scope.eliminar = function (item) {
            console.log(item);

            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre_capacitacion + " al profesor " + item.nombre_profesor + "?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANT_ASIGNACION_CAPACITACION",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.id },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        if (response.error == 0) {
                            $scope.alert.show = false;

                            $scope.alert = {
                                show: true,
                                title: "Alerta",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: 'Capacitacion eliminada correctamente',
                                close: function () {
                                    $scope.alert.show = false;
                                    $scope.volverTabla();
                                }
                            }

                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { console.log(e.toString()) })
                }
            }
        };
        //MANTENEDOR FIN

        //FUNCIONES
        $scope.abrirFormularioUsuario = function (opcion, item) {
            console.log(item);
            $scope.data.formulario = [];
            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;
            (opcion == "add") ? $scope.tituloFormularioUser = "Nueva " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            if (opcion == "edit") {
                console.log("item a editar ", item);
                $scope.camposEditar = true
                $scope.data.formulario.id = item.id;
                $scope.data.formulario.profesor = item.id_profesor;
                $scope.data.formulario.capacitacion = item.id_capacitacion;
                $scope.data.formulario.institucion = item.institucion;
                $scope.data.formulario.horas = item.horas * 1;

                angular.forEach($rootScope.listaEstadoCapacitacion, function (v) {
                    if (item.estado == v.value) {
                        $scope.data.formulario.estado = v;
                    }
                })

                angular.forEach($rootScope.listaEstadoCapacitacion, function (v) {
                    if (item.estado == v.value) {
                        $scope.data.formulario.estado = v;
                    }
                })

            }

            $scope.loadData();
        };

        $scope.volverTabla = function () {
            $scope.camposEditar = false;
            $scope.formNewUser = false;
            $scope.mostrarGuardar = false;
            $scope.mostrarEditar = false;
            $scope.tablaMantenedor = true;
            $scope.taskDetailTableParams.sorting({});
            input = document.getElementById("inputFile");
            input.value = ''
            $scope.loadData();
        };

        $scope.volverTablaDocument = function () {
            $scope.formDocument = false;
            $scope.tablaMantenedor = true;
            $scope.loadData();
        };

        $scope.verDocumento = function (item) {
            var input = {
                SP: "GET_BASE64",
                FILTERS: [{ value: item.id, type: "number" }]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.data.length > 0) {
                    $scope.datosSeleccionados = item
                    let pdfWindow = window.open("");
                    pdfWindow.document.write(
                        "<iframe width='100%' height='100%' src='" + encodeURI(response.data[0].base64) + "'></iframe>"
                    )
                }
            })
        };

        $scope.verDocumentoEdit = function () {
            var input = {
                SP: "GET_BASE64",
                FILTERS: [{ value: $scope.data.formulario.id, type: "number" }]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.data.length > 0) {
                    let pdfWindow = window.open("");
                    pdfWindow.document.write(
                        "<iframe width='100%' height='100%' src='" + encodeURI(response.data[0].base64) + "'></iframe>"
                    )
                }
            })
        };

        $scope.agregarDocumento = function () {
            $scope.documento = "";
            //console.log($scope.documentoSeleccionado);
            $scope.documentForm = true;
        };

        $scope.subirDocumento = function () {

            var input = document.querySelector('input[type=file]');
            var file = input.files[0],
                reader = new FileReader();

            reader.onloadend = function () {
                var b64 = reader.result;
                $scope.documento = b64;

                var input = {
                    SP: "MANTENEDOR_DOCUMENTO",
                    FILTERS: [
                        { value: 0, type: "number" },
                        { value: $scope.documentoSeleccionado.id },
                        { value: $scope.documento }
                    ]
                }

                $http({
                    url: IPSERVICES + "post",
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    data: JSON.stringify(input),
                    headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                }).success(function (response) {
                    if (response.error == 0) {
                        $scope.alert.show = false;

                        $scope.alert = {
                            show: true,
                            title: "Mensaje",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.data[0].message,
                            close: function () {
                                $scope.alert.show = false;
                                $scope.documentForm = false;
                                $scope.volverTablaDocument();
                            }
                        }
                    } else {
                        $scope.alert.show = false;
                    }
                }).error(function (e) {
                    $scope.alert = {
                        show: true,
                        title: "Error",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: "Error con el servicio de MANTENEDOR_DOCUMENTO",
                        close: function () { $scope.alert.show = false; }
                    }
                })
            }
            reader.readAsDataURL(file);
        };

        $scope.eliminarDocumento = function () {
            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar el documento del RUT " + $scope.documentoSeleccionado.profesor + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {

                    var input = {
                        SP: "MANTENEDOR_DOCUMENTO",
                        FILTERS: [
                            { value: 1, type: "number" },
                            { value: $scope.documentoSeleccionado.id },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        if (response.error == 0) {
                            $scope.alert.show = false;

                            $scope.alert = {
                                show: true,
                                title: "Mensaje",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: response.data[0].message,
                                close: function () {
                                    $scope.alert.show = false;
                                    $scope.documentForm = false;
                                    $scope.volverTablaDocument();
                                }
                            }
                        } else {
                            $scope.alert.show = false;
                        }
                    }).error(function (e) {
                        $scope.alert = {
                            show: true,
                            title: "Error",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: "Error con el servicio de MANTENEDOR_DOCUMENTO",
                            close: function () { $scope.alert.show = false; }
                        }
                    })
                }
            }
        };

        $scope.convertToBase64 = function () {
            //Read File
            var selectedFile = document.getElementById("inputFile").files;
            //Check File is not Empty
            if (selectedFile.length > 0) {
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    $scope.data.formulario.documento = base64
                    //console.log(base64);
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);
            }
        }

        $scope.traerFechaHoy = function () {
            var fecha = new Date();
            return fecha.getFullYear() + "-" + ((fecha.getMonth() * 1) + 1) + "-" + fecha.getDate();
        };

        $scope.formatoFecha = function (fecha) {
            return fecha.getFullYear() + "-" + ((fecha.getMonth() * 1) + 1) + "-" + fecha.getDate();
        };

        $scope.formatoFechaDDMMYYY = function (fecha) {
            return fecha.getFullYear() + "-" + ((fecha.getMonth() * 1) + 1) + "-" + fecha.getDate();
        };

        $scope.validarFormulario = function () {
            var validar = true;
            if ($scope.data.formulario.capacitacion == "" || $scope.data.formulario.capacitacion == undefined || $scope.data.formulario.capacitacion == null && validar) {
                console.log("capa");
                validar = false;
            }

            if ($scope.data.formulario.profesor == "" || $scope.data.formulario.profesor == undefined || $scope.data.formulario.profesor == null && validar) {
                console.log("profe");
                validar = false;
            }
            return validar;
        }

        $scope.selectEstado = function (idEstado) {
            console.log(idEstado);
            if (idEstado == "0") { return "Rechazada" }
            else if (idEstado == "1") { return "Aceptada" }
            else if (idEstado == "2") { return "En Proceso" }
            else { return "N/A" }
        }
        //FUNCIONES FIN
    }
])

    .filter('customUserDateFilter', function ($filter) {
        return function (values, dateString) {
            var filtered = [];
            if (typeof values != 'undefined' && typeof dateString != 'undefined') {
                angular.forEach(values, function (value) {
                    var source = ($filter('date')(value.fecha, 'dd/MM/yyyy')).toLowerCase();
                    var temp = dateString.toLowerCase();
                    if (source.indexOf(temp) >= 0) {
                        filtered.push(value);

                    }
                });
            }
            return filtered;
        }
    })

    .filter('formatFecha', function () {
        return function (input) {
            if (input == "" || input == undefined || input == null) {
                return '';
            }
            try {
                var fechaProc = input.split(' ')[0];
                var dd = fechaProc.split('-')[2];
                var mm = fechaProc.split('-')[1];
                var yyyy = fechaProc.split('-')[0];
                return dd + "/" + mm + "/" + yyyy;
            } catch (error) {

            }
        }
    })