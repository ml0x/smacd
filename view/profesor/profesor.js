/* Setup blank page controller */
angular.module('MetronicApp').controller('profesor_ctr', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer', '$timeout', '$filter', 'ngTableParams',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer, $timeout, $filter, ngTableParams) {

        //VARIABLES
        $scope.alert = { loading: false };
        $scope.formNewUser = false;
        $scope.formNewCapacitacion = false;
        $scope.tablaMantenedor = true;
        $scope.tablaEvaluacion = false;
        $scope.cabecera = $rootScope.datosUsuario;
        $scope.vistaCapacitaciones = false;
        $scope.mostrarDocumento = false;
        $scope.data = {
            items: [],
            itemsCapacitaciones: [],
            formulario: {
                rut: "",
                nombre: "",
                apeliidO: '',
                correo: '',
                telefono: '',
                departamento: '',
                estado: '',
                nivel_academico: '',
                user: "",
                pass: "",
                profile: ""
            },
            formularioCapacitacion: {
                id: "",
                profesor: "",
                nombre: '',
                institucion: '',
                horas: '',
                fecha: '',
                documento: '',
                capacitacion: '',
            }
        };
        //VARIABLES FIN

        //MANTENEDOR
        $scope.loadData = function () {
            $scope.alert = { loading: true };
            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 0, type: "number" },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.items = [];
                    angular.forEach(response.data, function (v, k) {
                        $scope.data.items.push(v);
                    });

                    $scope.taskCheckbox = { "isNormal": true };
                    $scope.taskDetailData = $scope.data.items;

                    $scope.resetNgTable = function () {
                        return {
                            total: $scope.taskDetailData.length, // length of data
                            getData: function ($defer, params) {
                                var filters = params.filter();
                                var tempDateFilter;
                                var orderedData = params.sorting() ? $filter('orderBy')($scope.taskDetailData, params.orderBy()) : $scope.taskDetailData;

                                if (filters) {
                                    if (filters.fecha) {
                                        orderedData = $filter('customUserDateFilter')(orderedData, filters.fecha);
                                        tempDateFilter = filters.fecha;
                                        delete filters.fecha;
                                    }

                                    orderedData = $filter('filter')(orderedData, filters);
                                    filters.fecha = tempDateFilter;
                                }

                                var table_data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                params.total(orderedData.length); // set total for recalc pagination
                                $defer.resolve(table_data);
                                $scope.alert = { loading: false };
                            }
                        }
                    }

                    $scope.taskDetailTableParams = new ngTableParams({
                        page: 1, // show first page
                        count: 10, // count per page
                        sorting: {
                            index: 'asc' // initial sorting
                        }
                    }, $scope.resetNgTable());

                    setTimeout(() => {
                        $scope.taskDetailTableParams.sorting({});
                        $scope.$apply();
                        $scope.alert = { loading: false };
                    }, 500);

                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_PROFESOR.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        };

        $scope.loadData();

        $scope.agregar = function () {

            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: $scope.data.formulario.rut },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.correo },
                    { value: $scope.data.formulario.telefono },
                    { value: $scope.data.formulario.departamento.objeto.id },
                    { value: $scope.data.formulario.estado.value },
                    { value: $scope.data.formulario.nivel_academico.objeto.id },
                    { value: $scope.data.formulario.user },
                    { value: $scope.data.formulario.pass },
                    { value: $scope.data.formulario.profile.value },
                ]
            }

            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                console.log(response)
                if (response.error == 0) {
                    angular.forEach($rootScope.listaCapacitacionBase, function (v) {
                        var input = {
                            SP: "MANT_ASIGNACION_CAPACITACION",
                            FILTERS: [
                                { value: 5, type: "number" },
                                { value: null },
                                { value: v.id },
                                { value: $scope.data.formulario.rut },
                                { value: "LAUREATE CENTER FOR GLOBAL FACULTY EXCELLENCE" },
                                { value: null },
                                { value: $scope.traerFechaHoy() },
                                { value: null },
                                { value: $rootScope.listaEstadoCapacitacionProfe[0].value },
                            ]
                        }
                        $http({
                            url: IPSERVICES + "post",
                            method: 'POST',
                            dataType: 'json',
                            timeout: 10000,
                            data: JSON.stringify(input),
                            headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                        }).success(function (response2) {
                            console.log(response2)
                            //if (response.error == 0) { console.log(response.data[0].message) } else { console.log(response.message) }
                        }).error(function (e) { console.log(e.toString()) })
                    })

                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_PROFESOR.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })

        };

        $scope.editar = function () {

            var validar = $scope.validarFormulario();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANTENEDOR_PROFESOR",
                FILTERS: [
                    { value: 2, type: "number" },
                    { value: $scope.data.formulario.rut },
                    { value: $scope.data.formulario.nombre },
                    { value: $scope.data.formulario.correo },
                    { value: $scope.data.formulario.telefono },
                    { value: $scope.data.formulario.departamento.objeto.id },
                    { value: $scope.data.formulario.estado.value },
                    { value: $scope.data.formulario.nivel_academico.objeto.id },
                    { value: $scope.data.formulario.user },
                    { value: $scope.data.formulario.pass },
                    { value: $scope.data.formulario.profile.value }

                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTabla();
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_PROFESOR.',
                    close: function () {
                        $scope.alert.show = false;
                        $scope.volverTabla();
                    }
                }
            })
        };

        $scope.eliminar = function (item) {
            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre + ' ' + item.rut + " ?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANTENEDOR_PROFESOR",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.rut },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null }
                        ]
                    }
                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        //console.log(response)
                        if (response.error == 0) {
                            $scope.alert.show = false;
                            $scope.loadData();
                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { })
                }
            }
        };
        //MANTENEDOR FIN

        //FUNCIONES 
        $scope.traerFechaHoy = function () {
            var fecha = new Date();
            return fecha.getFullYear() + "-" + ((fecha.getMonth() * 1) + 1) + "-" + fecha.getDate();
        };

        $scope.abrirFormularioUsuario = function (opcion, item) {
            console.log(item);
            $scope.data.formulario = [];

            $scope.formNewUser = true;
            $scope.tablaMantenedor = false;
            (opcion == "add") ? $scope.tituloFormularioUser = "Nuevo " : $scope.tituloFormularioUser = "Editar ";
            (opcion == "add") ? $scope.mostrarGuardar = true : $scope.mostrarEditar = true;

            angular.forEach($rootScope.listaDepartamentos, function (v) {
                if (v.objeto.id == item.departamento) {
                    item.departamento = v;
                }
            })

            angular.forEach($rootScope.listaEstados, function (v) {
                if (v.value == item.estado) {
                    item.estado = v;
                }
            })


            if (opcion == "edit") {
                $scope.data.formulario.rut = item.rut;
                $scope.data.formulario.nombre = item.nombre;
                $scope.data.formulario.correo = item.correo;
                $scope.data.formulario.telefono = item.telefono;
                $scope.data.formulario.departamento = item.departamento;
                //$scope.data.formulario.nivel_academico = item.nivel_academico;
                $scope.data.formulario.estado = item.estado;
                $scope.data.formulario.user = item.user;
                $scope.data.formulario.pass = item.pass;
                //$scope.data.formulario.estado = item.estado;



                angular.forEach($rootScope.listaNivelAcademico, function (v) {
                    if (item.nivel_academico == v.objeto.id) {
                        $scope.data.formulario.nivel_academico = v;
                    }
                })

                angular.forEach($rootScope.listaPerfil, function (v) {
                    if (item.profile == v.value) {
                        $scope.data.formulario.profile = v;
                    }
                })
            }

            $scope.loadData();
        };

        $scope.volverTabla = function () {
            $scope.formNewUser = false;
            $scope.mostrarGuardar = false;
            $scope.mostrarEditar = false;
            $scope.tablaMantenedor = true;
            $scope.taskDetailTableParams.sorting({});
            $scope.loadData();
        };

        $scope.traerEvaluaciones = function (item) {
            $scope.alert = { loading: true };
            $scope.tituloEvalauciones = item.nombre + " " + item.rut;
            $scope.listaEvaluaciones = [];
            $scope.listaEvaluacionesPromedio = [];
            $scope.listaEvaluacionesPromedio = [0, 0, 0, 0, 0, 0, 0];
            $scope.arrayGrafico = [
                ['FECHA', 'P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7'],
            ];

            var input = {
                SP: "GET_LIST_EVALUATION_BY_RUT",
                FILTERS: [
                    { value: item.rut, type: "number" }
                ]
            }

            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                $scope.alert = { loading: false };
                //console.log(response)
                if (response.error == 0) {

                    if (response.data.length == 0) {
                        $scope.alert = {
                            show: true,
                            title: "Alerta",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: "No se han encontrado evaluaciones con el RUT: " + item.rut + ".",
                            close: function () {
                                $scope.alert.show = false;
                            }
                        }

                        return;
                    }

                    $scope.tablaMantenedor = false;
                    $scope.tablaEvaluacion = true;

                    angular.forEach(response.data, function (v, k) {
                        $scope.listaEvaluaciones.push({ nrc: v.nrc, fecha: v.fecha, p1: v.p1, p2: v.p2, p3: v.p3, p4: v.p4, p5: v.p5, p6: v.p6, p7: v.p7 })
                        $scope.listaEvaluacionesPromedio[0] = $scope.listaEvaluacionesPromedio[0] + v.p1 * 1;
                        $scope.listaEvaluacionesPromedio[1] = $scope.listaEvaluacionesPromedio[1] + v.p2 * 1;
                        $scope.listaEvaluacionesPromedio[2] = $scope.listaEvaluacionesPromedio[2] + v.p3 * 1;
                        $scope.listaEvaluacionesPromedio[3] = $scope.listaEvaluacionesPromedio[3] + v.p4 * 1;
                        $scope.listaEvaluacionesPromedio[4] = $scope.listaEvaluacionesPromedio[4] + v.p5 * 1;
                        $scope.listaEvaluacionesPromedio[5] = $scope.listaEvaluacionesPromedio[5] + v.p6 * 1;
                        $scope.listaEvaluacionesPromedio[6] = $scope.listaEvaluacionesPromedio[6] + v.p7 * 1;
                    })

                    angular.forEach($scope.listaEvaluaciones, function (v, k) {
                        $scope.arrayGrafico.push([v.nrc, v.p1 * 1, v.p2 * 1, v.p3 * 1, v.p4 * 1, v.p5 * 1, v.p6 * 1, v.p7 * 1]);
                    })

                    google.charts.load('current', { 'packages': ['corechart'] });
                    google.charts.setOnLoadCallback(drawVisualization);

                    function drawVisualization() {
                        var data = google.visualization.arrayToDataTable($scope.arrayGrafico);

                        var options = {
                            title: 'Evaluaciones del Profesor',
                            vAxis: { title: 'Puntaje', minValue: 0 },
                            hAxis: { title: 'NRC', format: "yyy-mm-dd" },
                            seriesType: 'bars',
                            series: { 5: { type: 'line' } }
                        };
                        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                        chart.draw(data, options);
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }

            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio GET_LIST_EVALUATION_BY_ID.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })

        };

        $scope.taerCapacitaciones = function (item) {
            $scope.profesorSeleccionado = item.nombre;
            $scope.profesorSeleccionadoRut = item.rut;
            $scope.listaCapacitaciones = [];

            $scope.tablaMantenedor = false;
            $scope.vistaCapacitaciones = true;

            $scope.alert = { loading: true };
            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 4, type: "number" },
                    { value: null },
                    { value: null },
                    { value: $scope.profesorSeleccionadoRut },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.itemsCapacitaciones = [];
                    angular.forEach(response.data, function (v, k) {


                        var nombre_profesor = "";
                        var nombre_capacitacion = "";
                        angular.forEach($rootScope.listaProfesores, function (profesor) {
                            if (v.id_profesor == profesor.objeto.rut) {
                                nombre_profesor = profesor.objeto.nombre;
                            }
                        })

                        angular.forEach($rootScope.listaCapacitacion, function (capacitacion) {
                            if (v.id_capacitacion == capacitacion.objeto.id) {
                                nombre_capacitacion = capacitacion.objeto.descripcion;
                            }
                        })

                        $scope.data.itemsCapacitaciones.push(
                            {
                                id: v.id * 1,
                                id_profesor: v.id_profesor,
                                nombre_profesor: nombre_profesor,
                                id_capacitacion: v.id_capacitacion,
                                nombre_capacitacion: nombre_capacitacion,
                                institucion: v.institucion,
                                horas: v.horas * 1,
                                fecha: v.fecha,
                                estado: v.estado,
                                desEstado: $scope.selectEstado(v.estado)
                            }
                        )
                    });

                    $scope.alert = { loading: false };
                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })

        };

        $scope.volverTablaEvaluacion = function () {
            $scope.tablaEvaluacion = false;
            $scope.vistaCapacitaciones = false;
            $scope.tablaMantenedor = true;
        };

        $scope.verDocumentoCapacitacion = function (item) {

            var input = {
                SP: "GET_BASE64",
                FILTERS: [
                    { value: item.id, type: "number" }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                if (response.data.length > 0) {
                    $scope.datosSeleccionados = item
                    let pdfWindow = window.open("")
                    pdfWindow.document.write(
                        "<iframe width='100%' height='100%' src='" + encodeURI(response.data[0].base64) + "'></iframe>"
                    )
                }

            })
        };

        $scope.convertToBase64 = function () {
            //Read File
            var selectedFile = document.getElementById("inputFile").files;
            //Check File is not Empty
            if (selectedFile.length > 0) {
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    $scope.data.formularioCapacitacion.documento = base64
                    //console.log(base64);
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);
            }
        }

        $scope.agregarCapacitacion = function () {

            var validar = $scope.validarFormularioCapacitacion();
            if (!validar) {
                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes Completar todos los campos del formulario para continuar",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
                return;
            }

            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 1, type: "number" },
                    { value: null },
                    { value: $scope.data.formularioCapacitacion.capacitacion.objeto.id },
                    { value: $scope.profesorSeleccionadoRut },
                    { value: $scope.data.formularioCapacitacion.institucion },
                    { value: $scope.data.formularioCapacitacion.horas },
                    { value: $scope.traerFechaHoy() },
                    { value: $scope.data.formularioCapacitacion.documento },
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                console.log(response)
                if (response.error == 0) {
                    $scope.alert = {
                        show: true,
                        title: "Mensaje",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.data[0].message,
                        close: function () {
                            $scope.alert.show = false;
                            $scope.volverTablaCapacitaciones()
                        }
                    }

                } else {
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            }).error(function (e) {
                $scope.alert = {
                    show: true,
                    title: "Alerta",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: 'Error de conexion con servicio MANTENEDOR_CAPACITACION.',
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            })
        };

        $scope.eliminarCapacitacion = function (item) {
            $scope.alert = {
                show: true,
                title: "Confirmación",
                buttonPrint: false,
                buttonNewGuide: false,
                buttonAcept: true,
                buttonClose: true,
                message: "¿Desea Eliminar a " + item.nombre_capacitacion + " al profesor " + item.nombre_profesor + "?",
                close: function () {
                    $scope.alert.show = false;
                },
                accept: function () {
                    var input = {
                        SP: "MANT_ASIGNACION_CAPACITACION",
                        FILTERS: [
                            { value: 3, type: "number" },
                            { value: item.id },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null },
                            { value: null }
                        ]
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: JSON.stringify(input),
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
                    }).success(function (response) {
                        if (response.error == 0) {
                            $scope.alert.show = false;

                            $scope.alert = {
                                show: true,
                                title: "Alerta",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: 'Capacitacion eliminada correctamente',
                                close: function () {
                                    $scope.alert.show = false;
                                    $scope.volverTablaCapacitaciones()
                                }
                            }

                        } else {
                            alert(response.message);
                            $scope.alert.show = false;
                        }
                    }).error(function (e) { console.log(e.toString()) })
                }
            }
        };

        $scope.actualizarCapacitaciones = function () {

            $scope.listaCapacitaciones = [];

            $scope.tablaMantenedor = false;
            $scope.vistaCapacitaciones = true;

            $scope.alert = { loading: true };
            var input = {
                SP: "MANT_ASIGNACION_CAPACITACION",
                FILTERS: [
                    { value: 4, type: "number" },
                    { value: null },
                    { value: null },
                    { value: $scope.profesorSeleccionadoRut },
                    { value: null },
                    { value: null },
                    { value: null },
                    { value: null }
                ]
            }
            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: JSON.stringify(input),
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' }
            }).success(function (response) {
                //console.log(response)
                if (response.error == 0) {
                    $scope.data.itemsCapacitaciones = [];
                    angular.forEach(response.data, function (v, k) {


                        var nombre_profesor = "";
                        var nombre_capacitacion = "";
                        angular.forEach($rootScope.listaProfesores, function (profesor) {
                            if (v.id_profesor == profesor.objeto.rut) {
                                nombre_profesor = profesor.objeto.nombre;
                            }
                        })

                        angular.forEach($rootScope.listaCapacitacion, function (capacitacion) {
                            if (v.id_capacitacion == capacitacion.objeto.id) {
                                nombre_capacitacion = capacitacion.objeto.descripcion;
                            }
                        })

                        $scope.data.itemsCapacitaciones.push(
                            {
                                id: v.id * 1,
                                id_profesor: v.id_profesor,
                                nombre_profesor: nombre_profesor,
                                id_capacitacion: v.id_capacitacion,
                                nombre_capacitacion: nombre_capacitacion,
                                institucion: v.institucion,
                                horas: v.horas * 1,
                                fecha: v.fecha,
                            }
                        )
                    });

                    $scope.alert = { loading: false };
                } else {
                    alert(response.message);
                    $scope.alert = { loading: false };
                }
            }).error(function (e) { })

        };

        $scope.abrirFormularioCapacitacion = function (opcion, item) {
            $scope.data.formularioCapacitacion = [];
            $scope.formNewCapacitacion = true;
            $scope.vistaCapacitaciones = false;
            $scope.mostrarGuardar = true;
        };

        $scope.volverTablaCapacitaciones = function () {
            $scope.formNewCapacitacion = false;
            $scope.formNewCapacitacion = false;
            $scope.vistaCapacitaciones = true;
            $scope.actualizarCapacitaciones();
        }

        $scope.verGrupo = function (grupo) {
            $scope.learningPath = [];
            angular.forEach($rootScope.listaCapacitacion, function (capacitacion) {
                if (grupo.objeto.id == capacitacion.objeto.grupo) {
                    cont = 0;
                    angular.forEach($scope.data.itemsCapacitaciones, function (v) {
                        if (v.id_capacitacion == capacitacion.objeto.id) {
                            cont++;
                        }
                    })
                    $scope.learningPath.push({ text: capacitacion.text, background: (cont > 0) ? "#00800066" : "#ff000036" })
                }
            })

            $scope.alert = { title: grupo.text, learningPath: true, learningPathItems: $scope.learningPath }

        }

        $scope.validarFormulario = function () {

            var validar = true;

            if (validar) {
                var regexPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
                validar = regexPass.test($scope.data.formulario.pass)
                console.log(validar);
            }

            if ($scope.data.formulario.correo == "" || $scope.data.formulario.correo == undefined || $scope.data.formulario.correo == null && validar) {
                validar = false;
            }
            /*if (validar) {
                var regexCorreo = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
                validar = regexCorreo.test($scope.data.formulario.correo)
                console.log(validar);
            }*/

            if ($scope.data.formulario.rut == "" || $scope.data.formulario.rut == undefined || $scope.data.formulario.rut == null && $scope.data.formulario.rut.length != 9 && validar) {
                validar = false;
            }

            if ($scope.data.formulario.nombre == "" || $scope.data.formulario.nombre == undefined || $scope.data.formulario.nombre == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.telefono == "" || $scope.data.formulario.telefono == undefined || $scope.data.formulario.telefono == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.departamento == "" || $scope.data.formulario.departamento == undefined || $scope.data.formulario.departamento == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.estado == "" || $scope.data.formulario.estado == undefined || $scope.data.formulario.estado == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.nivel_academico == "" || $scope.data.formulario.nivel_academico == undefined || $scope.data.formulario.nivel_academico == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.user == "" || $scope.data.formulario.user == undefined || $scope.data.formulario.user == null && validar) {
                validar = false;
            }

            if ($scope.data.formulario.profile == "" || $scope.data.formulario.profile == undefined || $scope.data.formulario.profile == null && validar) {
                validar = false;
            }

            return validar;
        }

        $scope.validarFormularioCapacitacion = function () {
            var validar = true;

            if ($scope.data.formularioCapacitacion.capacitacion == "" || $scope.data.formularioCapacitacion.capacitacion == undefined || $scope.data.formularioCapacitacion.capacitacion == null && validar) {
                validar = false;
            }

            if ($scope.data.formularioCapacitacion.institucion == "" || $scope.data.formularioCapacitacion.institucion == undefined || $scope.data.formularioCapacitacion.institucion == null && validar) {
                validar = false;
            }

            if ($scope.data.formularioCapacitacion.horas == "" || $scope.data.formularioCapacitacion.horas == undefined || $scope.data.formularioCapacitacion.horas == null && validar) {
                validar = false;
            }

            if ($scope.data.formularioCapacitacion.documento == "" || $scope.data.formularioCapacitacion.documento == undefined || $scope.data.formularioCapacitacion.documento == null && validar) {
                validar = false;
            }

            return validar;
        }
        $scope.selectEstado = function (idEstado) {
            console.log(idEstado);
            if (idEstado == "0") { return "Rechazada" }
            else if (idEstado == "1") { return "Aceptada" }
            else if (idEstado == "2") { return "En Proceso" }
            else { return "N/A" }
        }
        //FUNCIONES FIN
    }
])

    .filter('formatFecha', function () {
        return function (input) {
            if (input == "" || input == undefined || input == null) {
                return '';
            }
            try {
                var fechaProc = input.split(' ')[0];
                var dd = fechaProc.split('-')[2];
                var mm = fechaProc.split('-')[1];
                var yyyy = fechaProc.split('-')[0];
                return dd + "/" + mm + "/" + yyyy;
            } catch (error) {

            }
        }
    })