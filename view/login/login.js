angular.module('MetronicApp').controller('Login', ['$rootScope', '$scope', 'settings', '$http', '$sce', '$location', 'dataToServer',
    function ($rootScope, $scope, settings, $http, $sce, $location, dataToServer) {
        $rootScope.fondoApp = "bg-cyan";
        $rootScope.user = { user: '', pass: '' };
        $rootScope.datosUsuario = {}
        $rootScope.listaEstados = [{ text: 'Inactivo', value: 0 }, { text: 'Activo', value: 1 }]
        var md = new MobileDetect(
            'Mozilla/5.0 (Linux; U; Android 4.0.3; en-in; SonyEricssonMT11i' +
            ' Build/4.1.A.0.562) AppleWebKit/534.30 (KHTML, like Gecko)' +
            ' Version/4.0 Mobile Safari/534.30');
        $rootScope.volver = function () {
            $location.path('/menu');
        }
        $rootScope.dispositivo = md.tablet();
        $scope.alert = { loading: false };

        $rootScope.traerData = function () {
            $scope.alert = { loading: true };
            $rootScope.listaDepartamentos = [];
            $rootScope.listaCursos = [];
            $rootScope.listaSedes = [];
            $rootScope.listaContratos = [];
            $rootScope.listaNivelAcademico = [];
            $rootScope.listaProfesores = [];
            $rootScope.listaGrupoCapacitacion = [];
            $rootScope.listaCapacitacion = [];
            $rootScope.listaCapacitacionBase = [];

            $rootScope.listaPerfil = [
                { value: 0, text: "Usuario" },
                { value: 1, text: "Administrador" },
            ];
            $rootScope.base = [
                { value: 0, text: "No" },
                { value: 1, text: "Si" },
            ];

            $rootScope.listaEstadoCapacitacion = [
                { value: 0, text: "Rechazado" },
                { value: 1, text: "Aceptado" },
                { value: 2, text: "En Proceso" },
            ];

            $rootScope.listaEstadoCapacitacionProfe = [
                { value: 2, text: "En Proceso" },
            ];

            var input = {
                SP: "GET_DEPARTAMENTOS",
                FILTERS: []
            }

            $http({
                url: IPSERVICES + "post",
                method: 'POST',
                dataType: 'json',
                timeout: 10000,
                data: input,
                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
            }).success(function (response) {
                $scope.alert = { loading: false };
                if (response.error == 0) {
                    angular.forEach(response.data, function (v, k) {
                        $rootScope.listaDepartamentos.push({ text: v.nombre, value: k, objeto: v })
                    })

                    var input2 = {
                        SP: "GET_CONTRATOS",
                        FILTERS: []
                    }

                    $http({
                        url: IPSERVICES + "post",
                        method: 'POST',
                        dataType: 'json',
                        timeout: 10000,
                        data: input2,
                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                    }).success(function (response) {
                        $scope.alert = { loading: false };
                        if (response.error == 0) {
                            angular.forEach(response.data, function (v, k) {
                                $rootScope.listaContratos.push({ text: v.nombre, value: k, objeto: v })
                            })

                            var input3 = {
                                SP: "GET_CURSOS",
                                FILTERS: []
                            }

                            $http({
                                url: IPSERVICES + "post",
                                method: 'POST',
                                dataType: 'json',
                                timeout: 10000,
                                data: input3,
                                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                            }).success(function (response) {
                                $scope.alert = { loading: false };
                                if (response.error == 0) {
                                    angular.forEach(response.data, function (v, k) {
                                        $rootScope.listaCursos.push({ text: v.nombre, value: k, objeto: v })
                                    })
                                    var input4 = {
                                        SP: "GET_NIVEL_ACADEMICO",
                                        FILTERS: []
                                    }

                                    $http({
                                        url: IPSERVICES + "post",
                                        method: 'POST',
                                        dataType: 'json',
                                        timeout: 10000,
                                        data: input4,
                                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                                    }).success(function (response) {
                                        $scope.alert = { loading: false };
                                        if (response.error == 0) {
                                            angular.forEach(response.data, function (v, k) {
                                                $rootScope.listaNivelAcademico.push({ text: v.nombre, value: k, objeto: v })
                                            })
                                            var input5 = {
                                                SP: "GET_PROFESORES",
                                                FILTERS: []
                                            }
                                            $http({
                                                url: IPSERVICES + "post",
                                                method: 'POST',
                                                dataType: 'json',
                                                timeout: 10000,
                                                data: input5,
                                                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                                            }).success(function (response) {
                                                $scope.alert = { loading: false };
                                                if (response.error == 0) {
                                                    angular.forEach(response.data, function (v, k) {
                                                        $rootScope.listaProfesores.push({ text: v.nombre, value: k, objeto: v })
                                                    })

                                                    var input6 = {
                                                        SP: "GET_SEDES",
                                                        FILTERS: []
                                                    }

                                                    $http({
                                                        url: IPSERVICES + "post",
                                                        method: 'POST',
                                                        dataType: 'json',
                                                        timeout: 10000,
                                                        data: input6,
                                                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                                                    }).success(function (response) {
                                                        $scope.alert = { loading: false };
                                                        if (response.error == 0) {
                                                            angular.forEach(response.data, function (v, k) {
                                                                $rootScope.listaSedes.push({ text: v.nombre, value: k, objeto: v })
                                                            })

                                                            var input7 = {
                                                                SP: "GET_GRUPO",
                                                                FILTERS: []
                                                            }
                                                            $http({
                                                                url: IPSERVICES + "post",
                                                                method: 'POST',
                                                                dataType: 'json',
                                                                timeout: 10000,
                                                                data: input7,
                                                                headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                                                            }).success(function (response) {
                                                                $scope.alert = { loading: false };
                                                                if (response.error == 0) {
                                                                    angular.forEach(response.data, function (v, k) {
                                                                        $rootScope.listaGrupoCapacitacion.push({ text: v.descripcion, value: k, objeto: v })
                                                                    })
                                                                    console.log($rootScope.listaGrupoCapacitacion);

                                                                    var input8 = {
                                                                        SP: "GET_CAPACITACION",
                                                                        FILTERS: []
                                                                    }
                                                                    $http({
                                                                        url: IPSERVICES + "post",
                                                                        method: 'POST',
                                                                        dataType: 'json',
                                                                        timeout: 10000,
                                                                        data: input8,
                                                                        headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                                                                    }).success(function (response) {
                                                                        $scope.alert = { loading: false };
                                                                        if (response.error == 0) {
                                                                            angular.forEach(response.data, function (v, k) {
                                                                                $rootScope.listaCapacitacion.push({ text: v.descripcion, value: k, objeto: v })
                                                                                if (v.base == "1") {
                                                                                    $rootScope.listaCapacitacionBase.push(v);
                                                                                }
                                                                            })
                                                                            console.log("Capacitaciones Base", $rootScope.listaCapacitacionBase);
                                                                        } else {
                                                                            contadorErrores++;
                                                                            $scope.alert = {
                                                                                show: true,
                                                                                title: "Error",
                                                                                buttonPrint: false,
                                                                                buttonNewGuide: false,
                                                                                buttonAcept: false,
                                                                                buttonClose: true,
                                                                                message: response.message,
                                                                                close: function () {
                                                                                    $scope.alert.show = false;
                                                                                }
                                                                            }
                                                                        }
                                                                    })

                                                                } else {
                                                                    contadorErrores++;
                                                                    $scope.alert = {
                                                                        show: true,
                                                                        title: "Error",
                                                                        buttonPrint: false,
                                                                        buttonNewGuide: false,
                                                                        buttonAcept: false,
                                                                        buttonClose: true,
                                                                        message: response.message,
                                                                        close: function () {
                                                                            $scope.alert.show = false;
                                                                        }
                                                                    }
                                                                }
                                                            })

                                                        } else {
                                                            contadorErrores++;
                                                            $scope.alert = {
                                                                show: true,
                                                                title: "Error",
                                                                buttonPrint: false,
                                                                buttonNewGuide: false,
                                                                buttonAcept: false,
                                                                buttonClose: true,
                                                                message: response.message,
                                                                close: function () {
                                                                    $scope.alert.show = false;
                                                                }
                                                            }
                                                        }
                                                    })

                                                } else {
                                                    contadorErrores++;
                                                    $scope.alert = {
                                                        show: true,
                                                        title: "Error",
                                                        buttonPrint: false,
                                                        buttonNewGuide: false,
                                                        buttonAcept: false,
                                                        buttonClose: true,
                                                        message: response.message,
                                                        close: function () {
                                                            $scope.alert.show = false;
                                                        }
                                                    }
                                                }
                                            })

                                        } else {
                                            contadorErrores++;
                                            $scope.alert = {
                                                show: true,
                                                title: "Error",
                                                buttonPrint: false,
                                                buttonNewGuide: false,
                                                buttonAcept: false,
                                                buttonClose: true,
                                                message: response.message,
                                                close: function () {
                                                    $scope.alert.show = false;
                                                }
                                            }
                                        }
                                    })

                                } else {
                                    contadorErrores++;
                                    $scope.alert = {
                                        show: true,
                                        title: "Error",
                                        buttonPrint: false,
                                        buttonNewGuide: false,
                                        buttonAcept: false,
                                        buttonClose: true,
                                        message: response.message,
                                        close: function () {
                                            $scope.alert.show = false;
                                        }
                                    }
                                }
                            })
                        } else {
                            contadorErrores++;
                            $scope.alert = {
                                show: true,
                                title: "Error",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: response.message,
                                close: function () {
                                    $scope.alert.show = false;
                                }
                            }
                        }
                    })

                } else {
                    contadorErrores++;
                    $scope.alert = {
                        show: true,
                        title: "Error",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: response.message,
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                }
            })
        }

        $scope.logear = function () {
            $scope.alert = { loading: true };
            if ($rootScope.user.user == undefined || $rootScope.user.user == ""
                || $rootScope.user.pass == undefined || $rootScope.user.pass == "") {

                $scope.alert = {
                    show: true,
                    title: "Error",
                    buttonPrint: false,
                    buttonNewGuide: false,
                    buttonAcept: false,
                    buttonClose: true,
                    message: "Debes ingresar los campos Usuario y Clave.",
                    close: function () {
                        $scope.alert.show = false;
                    }
                }
            } else {
                var input = {
                    SP: "LOGIN",
                    FILTERS: [
                        { value: $rootScope.user.user },
                        { value: $rootScope.user.pass }
                    ]
                }

                $http({
                    url: IPSERVICES + "post",
                    method: 'POST',
                    dataType: 'json',
                    timeout: 10000,
                    data: input,
                    headers: { 'Content-Type': "application/json", 'connection-properties': 'didd' },
                }).success(function (response) {
                    $scope.alert = { loading: false };
                    if (response.error == 0) {
                        if (response.data.length > 0) {
                            $rootScope.datosUsuario = response.data[0];
                            console.log($rootScope.datosUsuario);
                            $scope.traerData();
                            $location.path("/menu");
                        } else {
                            $scope.alert = {
                                show: true,
                                title: "Error",
                                buttonPrint: false,
                                buttonNewGuide: false,
                                buttonAcept: false,
                                buttonClose: true,
                                message: "Usuario y/o contraseña incorrecto.",
                                close: function () {
                                    $scope.alert.show = false;
                                }
                            }
                        }
                    } else {
                        $scope.alert = {
                            show: true,
                            title: "Error",
                            buttonPrint: false,
                            buttonNewGuide: false,
                            buttonAcept: false,
                            buttonClose: true,
                            message: response.message,
                            close: function () {
                                $scope.alert.show = false;
                            }
                        }
                    }
                }).error(function (e) {
                    $scope.alert = { loading: false };
                    $scope.alert = {
                        show: true,
                        title: "Alerta",
                        buttonPrint: false,
                        buttonNewGuide: false,
                        buttonAcept: false,
                        buttonClose: true,
                        message: 'Error de conexion con servicio LOGIN.',
                        close: function () {
                            $scope.alert.show = false;
                        }
                    }
                })
            }
        }
    }
]);