MetronicApp.directive("ag1Menu", function () {
    return {
        restrict: 'E',
        scope: {
            menu: '='
        },
        link: function (scope, element, attrs, alerts) {
            //console.log(scope, element, attrs, alerts);
        },
        templateUrl: 'js/directives/menus/menu.html',
        controller: ['$scope', '$http', '$interval', '$location', function ($scope, $location, $http, $interval) {
            //console.log($scope);
            $scope.alerta = {
                show: false,
                call: {}
            }
            $scope.alerta.call = function (fn, x, x2) {
                //console.log(fn, x, x2);
                $scope[fn](x, x2);
            };

            $scope.irCrearGuia = function () {
                $location.path('/crearGuia');
            }
        }]
    };
});